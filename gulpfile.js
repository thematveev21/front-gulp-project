import gulp from "gulp";
import sass from 'sass';
import gulpsass from 'gulp-sass';
import browserSync from "browser-sync";

const sassProcess = gulpsass(sass)
const browser = browserSync.create()

// sync
export function reloadBrowser(cb){

    browser.reload()

    cb()
}


// html

export function html(){
    return gulp.src('./src/**/*.html')
            .pipe(gulp.dest('./dist'))
}

// css preprocessors

export function css(){
    return gulp.src('./src/sass/main.scss')
            .pipe(sassProcess())
            .pipe(gulp.dest('./dist'))
}


export function images(){
    return gulp.src('./src/images/**/*.+(jpeg|jpg|png|svg|tiff|webp)')
            .pipe(gulp.dest('./dist/images'))
}

export const build = gulp.parallel(html, css, images)

export function watch(){
    browser.init({
        server: {
            baseDir: './dist'
        },
        notify: false
    })

    gulp.watch('./src/**/*.*', gulp.series(build, reloadBrowser))
}



